package mgcript;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.AlgorithmParameterSpec;

import javax.crypto.Cipher;
import javax.crypto.CipherOutputStream;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class Encripter {
 
            public String Skey;
    private final  int IV_LENGTH = 16; // Default length with Default 128
                                                // key AES encryption
    private final  int DEFAULT_READ_WRITE_BLOCK_BUFFER_SIZE = 1024;

    private final  String ALGO_RANDOM_NUM_GENERATOR = "SHA1PRNG";
    private final String ALGO_SECRET_KEY_GENERATOR = "AES";
    private final  String ALGO_VIDEO_ENCRYPTOR = "AES/CBC/PKCS5Padding";

    @SuppressWarnings("resource")
    public void encrypt(Key key, AlgorithmParameterSpec paramSpec, InputStream in, OutputStream out)
            throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException,
            InvalidAlgorithmParameterException, IOException {
        try {
            // byte[] iv = new byte[] { (byte) 0x8E, 0x12, 0x39, (byte) 0x9C,
            // 0x07, 0x72, 0x6F, 0x5A, (byte) 0x8E, 0x12, 0x39, (byte) 0x9C,
            // 0x07, 0x72, 0x6F, 0x5A };
            // AlgorithmParameterSpec paramSpec = new IvParameterSpec(iv);
            
            /*Key secretKey = new SecretKeySpec(key.getBytes(), ALGORITHM);
            Cipher cipher = Cipher.getInstance(TRANSFORMATION);
            cipher.init(cipherMode, secretKey);*/
            
            //Cipher c = Cipher.getInstance(ALGO_VIDEO_ENCRYPTOR);
            Cipher c = Cipher.getInstance("AES");
            //c.init(Cipher.ENCRYPT_MODE, key, paramSpec);
            c.init(Cipher.ENCRYPT_MODE, key, paramSpec);
            out = new CipherOutputStream(out, c);
            int count = 0;
            byte[] buffer = new byte[DEFAULT_READ_WRITE_BLOCK_BUFFER_SIZE];
            while ((count = in.read(buffer)) >= 0) {
                out.write(buffer, 0, count);
            }
        } finally {
            out.close();
        }
    }

    @SuppressWarnings("resource")
    public void decrypt(Key key, AlgorithmParameterSpec paramSpec, InputStream in, OutputStream out)
            throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException,
            InvalidAlgorithmParameterException, IOException {
        try {
            // byte[] iv = new byte[] { (byte) 0x8E, 0x12, 0x39, (byte) 0x9C,
            // 0x07, 0x72, 0x6F, 0x5A, (byte) 0x8E, 0x12, 0x39, (byte) 0x9C,
            // 0x07, 0x72, 0x6F, 0x5A };
            // AlgorithmParameterSpec paramSpec = new IvParameterSpec(iv);
            Cipher c = Cipher.getInstance("AES");
            c.init(Cipher.DECRYPT_MODE, key, paramSpec);
            out = new CipherOutputStream(out, c);
            int count = 0;
            byte[] buffer = new byte[DEFAULT_READ_WRITE_BLOCK_BUFFER_SIZE];
            while ((count = in.read(buffer)) >= 0) {
                out.write(buffer, 0, count);
            }
        } finally {
            out.close();
        }
    }
    public Encripter() {
             File inFile = new File(".\\");
         File outFile = new File(".\\");
         File outFile_dec = new File(".\\");
         
         try {
             
             Key key = new SecretKeySpec(Skey.getBytes(),"AES");
             //Key secretKey = new SecretKeySpec(key.getBytes(), ALGORITHM);
             //SecretKey key = KeyGenerator.getInstance(ALGO_SECRET_KEY_GENERATOR).generateKey();

             //byte[] keyData = key.getEncoded();
             //SecretKey key2 = new SecretKeySpec(keyData, 0, keyData.length, ALGO_SECRET_KEY_GENERATOR); //if you want to store key bytes to db so its just how to //recreate back key from bytes array

             byte[] iv = new byte[IV_LENGTH];
             SecureRandom.getInstance(ALGO_RANDOM_NUM_GENERATOR).nextBytes(iv); // If
                                                                                 // storing
                                                                                 // separately
             AlgorithmParameterSpec paramSpec = new IvParameterSpec(iv);

             this.encrypt(key, paramSpec, new FileInputStream(inFile), new FileOutputStream(outFile));
             this.decrypt(key, paramSpec, new FileInputStream(outFile), new FileOutputStream(outFile_dec));
         } catch (Exception e) {
             e.printStackTrace();
         }
    }

            public Encripter(String command,String fileName,String Key) {
                        // TODO Auto-generated constructor stub
                        File inFile = new File(fileName);
        File outFile = new File(fileName+".out");
        File outFile_dec = new File(fileName+".out2");
        Skey=Key;
        try {
             
             Key key = new SecretKeySpec(Skey.getBytes(),"AES");
             //Key secretKey = new SecretKeySpec(key.getBytes(), ALGORITHM);
            //SecretKey key = KeyGenerator.getInstance(ALGO_SECRET_KEY_GENERATOR).generateKey();

            //byte[] keyData = key.getEncoded();
            //SecretKey key2 = new SecretKeySpec(keyData, 0, keyData.length, ALGO_SECRET_KEY_GENERATOR); //if you want to store key bytes to db so its just how to //recreate back key from bytes array

           /* byte[] iv = new byte[IV_LENGTH];
            SecureRandom.getInstance(ALGO_RANDOM_NUM_GENERATOR).nextBytes(iv); // If
                                                                                // storing
                                                                                // separately
            AlgorithmParameterSpec paramSpec = new IvParameterSpec(iv);*/
            if (command.contains("enc")) {
                        this.encrypt(key, /*paramSpec*/null, new FileInputStream(inFile), new FileOutputStream(outFile));
            }
            if (command.contains("dec")) {
                        this.decrypt(key, /*paramSpec*/null, new FileInputStream(outFile), new FileOutputStream(outFile_dec));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
            }
}