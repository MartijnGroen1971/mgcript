package mgcript;

import java.io.File;

public class MgCript {

	public static void main(String[] args) {

		int IsError = 0;

		// read and validate parameters
		if (args.length != 3 && args[0] != "-enc" && args[0] != "-dec") {
			IsError = 1;
		} else {
			File f = new File(args[1]);
			if (!f.exists() || f.isDirectory()) {
				IsError = 2;
			} else if (args[2].length() != 16) {
				IsError = 3;
			}
		}
		if (IsError >0) {
			/* on error print help */
			System.out.println("command "+args[0]);
			System.out.println("filenaam "+args[1]);
			System.out.println("sleutel "+args[2]);
			System.out.println(String.valueOf(IsError));
			System.out.println(String.valueOf(args.length));
			System.out.println("Martijn Groens encryption Tool version 1.1 februari 2015");
			System.out.println("gebruik:");
			System.out.println("-enc filename keywithlength16 encrypts AES ");
			System.out.println("-dec filename samekeywithlength16 decrypts AES");
		} else {
			if (args[0].contentEquals("-enc")) {
				Encripter e = new Encripter("enc",args[1],args[2]);
			}
			if (args[0].contentEquals("-dec")) {
				Encripter e = new Encripter("dec",args[1],args[2]);
			}
		} 
	}
}
